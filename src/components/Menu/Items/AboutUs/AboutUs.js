import React from 'react';
import Menu from "../../Menu";
import './AboutUs.css'

const AboutUs = () => {
    return (
        <div>
            <Menu/>
            <div className="about-us">
                <div className="container">
                    <h3>About us</h3>
                    <div className="f-block">
                        <div className="we-are block">
                            <h4>Who We Are?</h4>
                            <p>Suspendisse dictum quam tortor, et dapibus lacus. Quisque euismod lacus in mi consequat
                                sed pulvinar elit ultricies. Nam eu ligula ut massa lobortis scelerisque. Curabitur at
                                rhoncus quam. In in tortor quis sem tempor ultricies in eu risus. Fusce posuere justo et
                                metus semper molestie.</p>
                        </div>
                        <div className="why-us block">
                            <h4>Why us?</h4>
                            <ul>
                                <li>Suspendisse dictum quam tortor</li>
                                <li>Set dapibus lacus.</li>
                                <li>Quisque euismod lacus in mi consequat sed</li>
                                <li>Nam eu ligula ut massa lobortis scelerisque.</li>
                                <li>Curabitur at rhoncus quam.</li>
                            </ul>
                        </div>
                        <div className="testemonials block">
                            <h4>Testemonials</h4>
                            <p className="ital">Curabitur at rhoncus quam. In in tortor quis sem tempor ultricies in eu
                                risus. Fusce posuere justo et metus semper molestie.</p>
                            <p className="name">Jhon Doe</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AboutUs;