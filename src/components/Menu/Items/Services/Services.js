import React from 'react';
import Menu from "../../Menu";
import './Services.css'

const Services = () => {
    return (
        <div>
            <Menu/>
           <h1>SERVICE</h1>
            <div className="services">
                <div className="container">
                    <h3>Services</h3>
                    <div className="f-block2">
                        <div className="consulting block2">
                            <i className="fas fa-comments"></i>
                            <h4>Consulting</h4>
                            <p>In quis magna eget magna rutrum hendrerit. Donec dap ibus aliquet magna in facilisis.</p>
                        </div>
                        <div className="planning block2">
                            <i className="fas fa-calendar-alt"></i>
                            <h4>Planning</h4>
                            <p>Curabitur at rhoncus quam. In in tortor quis sem tempor ultricies in.</p>
                        </div>
                        <div className="development block2">
                            <i className="fas fa-cogs"></i>
                            <h4>Development</h4>
                            <p>Aliquam tellus leo, egestas eget consectetur sed, lobortis vel nisl.</p>
                        </div>
                        <div className="production block2">
                            <i className="fas fa-flask"></i>
                            <h4>Production</h4>
                            <p>Curabitur at rhoncus quam. In in tortor quis sem tempor ultricies in.</p>
                        </div>
                        <div className="analysis block2">
                            <i className="fas fa-chart-bar"></i>
                            <h4>Analysis</h4>
                            <p>Curabitur at rhoncus quam. In in tortor quis sem tempor ultricies in.</p>
                        </div>
                        <div className="release block2">
                            <i className="fas fa-plane"></i>
                            <h4>Release</h4>
                            <p>Curabitur at rhoncus quam. In in tortor quis sem tempor ultricies in.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Services;