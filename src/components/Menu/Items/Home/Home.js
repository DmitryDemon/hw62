import React from 'react';
import Menu from "../../Menu";
import './Home.css'

const Home = () => {
    return (
        <div>
            <Menu/>
            <h1>HOME</h1>
        </div>
    );
};

export default Home;