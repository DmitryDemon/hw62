import React from 'react';
import {NavLink} from 'react-router-dom';
import './Menu.css';


const Menu = () => {
    return (
        <div className='header'>
            <h1>MENU</h1>
            <NavLink className='item' to='/'>Home</NavLink>
            <NavLink className='item' to='/aboutus'>About Us</NavLink>
            <NavLink className='item' to='/contacts' >Contacts</NavLink>
            <NavLink className='item' to='/services' >Services</NavLink>
        </div>
    );
};

export default Menu;