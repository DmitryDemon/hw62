import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import AboutUs from "./components/Menu/Items/AboutUs/AboutUs";
import Contacts from "./components/Menu/Items/Contacts/Contacts";
import Home from "./components/Menu/Items/Home/Home";
import Services from "./components/Menu/Items/Services/Services";


class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <Switch>
              <Route path="/" exact component={Home}/>
              <Route path="/aboutus" component={AboutUs}/>
              <Route path="/contacts" component={Contacts}/>
              <Route path="/services" component={Services}/>
            </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
